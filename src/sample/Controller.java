package sample;

import com.sun.org.apache.xml.internal.resolver.readers.ExtendedXMLCatalogReader;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

public class Controller implements Initializable{
    public Button aaa;
    public ProgressBar bbb;
    public ImageView iv;

    public void aaa(javafx.event.ActionEvent actionEvent) {
        Task task = new Task() {
            @Override
            protected Void call() throws Exception {
                final int max = 100;
                bbb.setVisible(true);
                for(int i = 1;i<max;i++){
                    Thread.sleep(100);
                    updateProgress(i,max);
                }
                return null;
            }
            @Override
            protected void succeeded() {
                super.succeeded();
                bbb.setVisible(false);
            }
            @Override
            protected void failed() {
                super.failed();
            }

        };


        bbb.setProgress(0.0);
        bbb.progressProperty().bind(task.progressProperty());
        new Thread(task).start();

        /*List<String> imagepath = new ArrayList<>();
        for(int i=1;i<6;i++){
            imagepath.add("/3064/".concat(String.valueOf(i)).concat(".png"));

        }*/
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);
        /*
        for(int i=0;i<5;i++){
            int j =i;
            timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(i + 1), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    iv.setImage(new Image(getClass().getResource(imagepath.get(j)).toString()));
                }
            }));
        }
        */
        IntStream.range(0,5).forEach(e ->{
            timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(e + 1), new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    iv.setImage(new Image(getClass().getResource("/3064/".concat(String.valueOf(e)).concat(".png")).toString()));
                }
            }));
        });
        timeline.play();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bbb.setVisible(false);
        //iv.setImage(new Image(getClass().getResource("/3064/1.png").toString()));
    }


}
